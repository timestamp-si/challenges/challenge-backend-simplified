package pt.timestamp.backend.challenge.post;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class PostService {

    // This
    public static final Map<String, String> posts = HashMap.empty();


    // This should receive as parameter the object CreatePostRequest (check the structure in README.md file)
    // example: public CompletableFuture<CreatePostResponse> create(final CreatePostRequest request, final Option<Boolean> sorted) {}
    //
    // If sorted is defined and is true, the return response should return the emojis list sorted
    public CompletableFuture<String> create(final Option<Boolean> sorted) {
        // TODO - Implement this
        return CompletableFuture.completedFuture(null);
    }


    // This should return the post with specified Id (id defined) from posts hashmap
    // This method should updated to: public CompletableFuture<CreatePostResponse> get(final UUID uuid)
    public CompletableFuture<String> get(final UUID uuid) {
        // TODO - Implement this
        return CompletableFuture.completedFuture(null);
    }
}
