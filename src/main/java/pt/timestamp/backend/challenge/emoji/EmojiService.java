package pt.timestamp.backend.challenge.emoji;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class EmojiService {

    // TODO - Implement this (String) should be replaced with new Object called EmojiDTO
    // example of method signature: public static final Set<EmojiDTO> emojis = HashSet.of(...)
    public static final Set<String> emojis = HashSet.of(
        ""
    );

    // This should return one EmojiDTO
    // example of method signature: public CompletableFuture<EmojiDTO> get(final UUID id)
    public CompletableFuture<String> get(final UUID id) {
        // TODO - Implement this
        return CompletableFuture.completedFuture(null);
    }
}
