package pt.timestamp.backend.challenge.user;

import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class UserService {

    // TODO - Implement this (String) should be replaced with new Object called UserDTO
    // example of method signature: public static final Set<UserDTO> users = HashSet.of(...)
    public static final Set<String> users = HashSet.of(
        ""
    );

    // This should return one UserDTO
    // example of method signature: public CompletableFuture<UserDTO> get(final UUID id)
    public CompletableFuture<String> get(final UUID id) {
        // TODO - Implement this
        return CompletableFuture.completedFuture(null);
    }

}
