# The Challenge
The idea behind this challenge is:
- Developer 3 microservices mocks (UserService, EmojiService and PostService)
- **UserService** is responsible for returning a user with the specified Id
- **EmojiService** is responsible for returning a emoji with the specified Id
- **PostService** is responsible for returning the object CreatePostResponse that aggregates the post, user, and emoji information (like user and emoji names) 


# Compile the App 
```shell
./mvnw clean compile
```

# Run the Tests
```shell
./mvnw test
```

## EmojiDTO Json (Translate structure into Java Object )
Note: The ***_comment** fields should be ignored, they are only used for comments
```json
{
  "id" : "1d4cde47-da1e-4a40-b77b-43d3ed4ee630",
  "id_comment" : "Should be an UUID",
  "name" : "Emoji Smile",
  "name_comment" : "Should have more than 5 characters"
}
```


## UserDTO Json (Translate structure into Java Object )
Note: The ***_comment** fields should be ignored, they are only used for comments
```json
{
  "id" : "1d4cde47-da1e-4a40-b77b-43d3ed4ee630",
  "id_comment" : "Should be an UUID",
  "name" : "Maria Maria",
  "name_comment" : "Should have more than 5 characters"
}
```

## CreatePostRequest (Translate structure into Java Object)
Note: The ***_comment** fields should be ignored, they are only used for comments
```json
{
  "id" : "1d4cde47-da1e-4a40-b77b-43d3ed4ee630", 
  "id_comment" : "Is not required, but when defined should assume the requested Id",
  "text" : "This is my first Post",
  "text_comment" : "This should not be empty or bigger than 100 characters",
  "userId" : "609a5106-79e7-4f61-8004-9931014e3bd9",
  "user_comment" : "Required",
  "emojiIds" : [
    "32177e56-1da9-45b3-bb08-fd10c26501f2",
    "73886f9a-e76a-44ea-b2e3-46be52a9a7c8",
    "81d6127d-6063-404e-adb3-380a94656625"
  ],
  "emojiIds_comment" : "Required",
  "public" : true,
  "public_comment" : "Optional, if not defined, assume TRUE"
}
```

## CreatePostResponse (Translate structure into Java Object)
Note: The ***_comment** fields should be ignored, they are only used for comments
```json
{
  "id" : "1d4cde47-da1e-4a40-b77b-43d3ed4ee630",
  "text" : "This is my first Post",
  "user" : {
    "id" : "609a5106-79e7-4f61-8004-9931014e3bd9",
    "name" : "Maria Maria"
  },
  "emojis" : [
    {
      "id": "32177e56-1da9-45b3-bb08-fd10c26501f2",
      "name" : "Emoji AAAA"
    },
    {
      "id" : "73886f9a-e76a-44ea-b2e3-46be52a9a7c8",
      "name" : "Emoji CCCC"
    },
    {
      "id" : "81d6127d-6063-404e-adb3-380a94656625",
      "name" : "Emoji ZZZZ"
    }
  ],
  "public" : true
}
```


# Notes: IMPORTANT!!!!
- Use Lists, Sets, HashSet, HashMap, Option, Future from Vavr https://docs.vavr.io/
- you should use Java functional Programming as  much as possible (Java + Vavr)
- you should use Reactive Programming between call services (CompletableFuture, Future (from Vavr)) (Java + Vavr)
- https://docs.vavr.io/
- You should develop the necessary tests
- You can check ![app_architecture.png](docs%2Fapp_architecture.png) for a better understanding of what you are trying to do

# TIP
- The Vavr List, Set, are almost equal to Java List, Set, but they are immutable 
- For parallel calls you can you Vavr **For** + Vavr **Future.sequence()**

